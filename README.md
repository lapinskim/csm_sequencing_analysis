Targeted RNA Knockdown by a Type III CRISPR-Cas Complex in Zebrafish
=================================================
Sequencing data pre-processing
------------------------------

CRISPR-Csm next-generation sequencing data processing utilizing [Nextflow](https://www.nextflow.io/) pipeline.

The analysis has been published in:

> Fricke, Thomas, Dalia Smalakyte, Maciej Lapinski, Abhishek Pateria, Charles Weige, Michal Pastor, Agnieszka Kolano, et al. 2020. “Targeted RNA Knockdown by a Type III CRISPR-Cas Complex in Zebrafish.” The CRISPR Journal 3 (4): 299–313. https://doi.org/10.1089/crispr.2020.0032.

To reach the data analysis sister repository please follow the link:

https://gitlab.com/lapinskim/csm_data_analysis

### Data availability
All sequencing data have been deposited in the GEO database under accession number [GSE146852](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE146852).

### Running the pipeline

#### Prerequisities

- [conda](https://docs.conda.io/en/latest/miniconda.html) >= 4.7.11
- [nextflow](https://www.nextflow.io/) >= 19.07.0.5106

**Note**: With Nextflow <= 19.07.0 place `~/miniconda3/bin/activate` in your PATH

#### Pipeline configuration

To specify the path to the directory containing your raw FASTQ files or the reference genome, please change the pipeline input parameters in the file `main.nf` to your liking.

The file `nextflow.config` contains additional process executor configuration that needs to be changed according to your execution environment.

#### Execution

In the cloned directory run:

```
nextflow run main.nf -resume
```

### Pipeline outline

![nextflow_graph][graph]

[graph]: ./images/nextflow_csm.png "Pipeline outline graph"

### Additional information

For more information about the pipeline see **Supplementary Methods** section of the accompanying paper.
