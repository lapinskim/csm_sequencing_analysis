#!/usr/bin/env nextflow

/* Pipeline for the analysis of the StCsm sequencing results
*
* IMPORTANT: With Nextflow <= 19.07.0 place ~/miniconda3/bin/activate
* in your PATH
*
*/

// Pipeline input parameters
params.fastq_dir = "./reads"
params.reference_dir = "./ref"
params.out_dir = "./data"
params.genome_fasta = "${params.reference_dir}/Danio_rerio.GRCz11.dna_sm.primary_assembly.fa"
params.annotation_gtf = "${params.reference_dir}/Danio_rerio.GRCz11.97.gtf"
params.egfp_fasta = "./egfp.fa"
params.egfp_gtf = "./egfp.gtf"
params.cdna_fasta = "${params.reference_dir}/Danio_rerio.GRCz11.97.cdna.all.fa"
params.ncrna_fasta = "${params.reference_dir}/Danio_rerio.GRCz11.97.ncrna.fa"
params.crrna_fasta = "./egfp_crrna.fa"

// Pipeline programs parameters
params.cutadapt_q = "20"
params.read_length = "59"

// Input channels

// Rep-4 was present in the SampleSheet, but not actually pooled for the
// sequencing run so we ignore it
Channel
  .fromFilePairs("${params.fastq_dir}/*-Rep-{1,2,3}*{_R1,R2}*.fastq.gz") {
    file ->
      file
        .getName()
        .replaceFirst(/_R[1,2]_.+$/, '')
  }
  .into {
    paired_fastq_ch1;
    paired_fastq_ch2
  }

Channel
  .fromPath(params.genome_fasta)
  .set { reference_genome_ch }

Channel
  .fromPath(params.annotation_gtf)
  .set { annotation_ch }

Channel
  .fromPath( [params.egfp_fasta, params.egfp_gtf] )
  .set { egfp_ch }

Channel
  .fromPath( [params.cdna_fasta, params.ncrna_fasta] )
  .set { reference_fasta_ch }

Channel
  .fromPath(params.crrna_fasta)
  .set { crrna_ch }

// Pipeline

// Perform initial quality control
process fastqc_initial {
  conda "bioconda::fastqc=0.11.8"
  
  cpus "2"
  memory "20GB"
  clusterOptions "-V -l h_vmem=10G"

  publishDir params.out_dir, mode: "copy"

  input:
  set sample_id, file(reads) from paired_fastq_ch1

  output:
  file "fastqc_initial/*.{zip,html}" into fastqc_ch

  """
  mkdir -v "./fastqc_initial"
  fastqc -o "./fastqc_initial" -t 2 -f fastq ${reads}
  """
}

process multiqc_initial {
  conda "bioconda::multiqc=1.7"

  cpus "1"
  memory "10GB"
  clusterOptions "-V -l h_vmem=10G"

  publishDir params.out_dir, mode: "copy"

  input:
  file "fastqc_initial/*" from fastqc_ch.collect()

  output:
  file "multiqc_initial/multiqc_report.html"
  file "multiqc_initial/multiqc_data"

  """
  mkdir -v "./multiqc_initial"
  multiqc -o "./multiqc_initial" .
  """
}

// Extract the UMI sequences from the first 12 nucleotides of the Read 1
process umi_extract {
  conda "bioconda::umi_tools=1.0.0"

  cpus "1"
  memory "20GB"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(reads) from paired_fastq_ch2

  output:
  set sample_id, file("umi_tools_extract/*_R1.extracted.fastq.gz"), file("umi_tools_extract/*_R2.extracted.fastq.gz") into extracted_paired_fastq_ch
  file "umi_tools_extract/*.extracted.log"
  file "umi_tools_extract/*.extracted.err"

  """
  mkdir -v "./umi_tools_extract"
  umi_tools extract \
    --stdin=${reads[0]} \
    --read2-in=${reads[1]} \
    --bc-pattern=NNNNNNNNNNNN \
    --stdout="./umi_tools_extract/${sample_id}_R1.extracted.fastq.gz" \
    --read2-out="./umi_tools_extract/${sample_id}_R2.extracted.fastq.gz" \
    --log="./umi_tools_extract/${sample_id}.extracted.log" \
    --error="./umi_tools_extract/${sample_id}.extracted.err";
  """
}

// Trim the processed reads based on adapter content and quality scores
process cutadapt {
  conda "bioconda::cutadapt=2.4"

  cpus "10"
  memory "40GB"
  clusterOptions "-V -l h_vmem=4G"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(read1), file(read2) from extracted_paired_fastq_ch

  output:
  set sample_id, file("cutadapt/*_R1.extracted.trimmed.fastq.gz"), file("cutadapt/*_R2.extracted.trimmed.fastq.gz") into trimmed_extracted_paired_fastq_ch1, trimmed_extracted_paired_fastq_ch2
  file "cutadapt/*.log" into cutadapt_log_ch

  """
  mkdir -v "./cutadapt"
  cutadapt \
    --cores 10 \
    -a "AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC;min_overlap=3" \
    -A "N{12}AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT;min_overlap=16" \
    --error-rate 0.1 \
    --quality-cutoff ${params.cutadapt_q},0 \
    --nextseq-trim ${params.cutadapt_q} \
    --minimum-length 10 \
    --pair-filter any \
    --output "./cutadapt/${sample_id}_R1.extracted.trimmed.fastq.gz" \
    --paired-output "./cutadapt/${sample_id}_R2.extracted.trimmed.fastq.gz" \
    ${read1} \
    ${read2} \
    | tee "cutadapt/${sample_id}.log";
  """
}

// Perform quality control on the processed reads
process fastqc_processed {
  conda "bioconda::fastqc=0.11.8"
  
  cpus "2"
  memory "20GB"
  clusterOptions "-V -l h_vmem=10G"

  publishDir params.out_dir, mode: "copy"

  input:
  set sample_id, file(read1), file(read2) from trimmed_extracted_paired_fastq_ch1

  output:
  file "fastqc_processed/*.{zip,html}" into fastqc_procesed_ch

  """
  mkdir -v "./fastqc_processed"
  fastqc -o "./fastqc_processed" -t 2 -f fastq ${read1} ${read2}
  """
}

// Add EGFP sequence and annotation to the reference
process egfp_reference {

  cpus "1"
  memory "10GB"

  input:
  set egfp_fasta, egfp_gtf from egfp_ch.collect()
  file genome_fasta from reference_genome_ch
  file annotation_gtf from annotation_ch

  output:
  file "genome_egfp.fa" into genome_egfp_ch1, genome_egfp_ch2, genome_egfp_ch3
  file "${annotation_name}.gtf" into annotation_egfp_ch1, annotation_egfp_ch2

  script:
  annotation_name = annotation_gtf.getBaseName() + ".EGFP"
  """
  cat ${genome_fasta} ${egfp_fasta} >"genome_egfp.fa"
  cat ${annotation_gtf} ${egfp_gtf} >"${annotation_name}.gtf"
  """
}

// Index the reference genome with STAR
process STAR_index {
  conda "bioconda::star=2.7.2b"

  cpus "20"
  memory "64GB"

  storeDir params.reference_dir

  input:
  file genome_fasta from genome_egfp_ch1
  file annotation_gtf from annotation_egfp_ch1

  output:
  file "${index_dir}" into star_index_ch

  script:
  star_oh = params.read_length.toInteger() - 1
  index_name = annotation_gtf.getBaseName()
  index_dir = index_name + ".STAR.OH" + star_oh.toString() + ".index"
  """
  mkdir -v "${index_dir}"
  STAR \
   --runThreadN 20 \
   --runMode genomeGenerate \
   --genomeDir "./${index_dir}" \
   --genomeFastaFiles ${genome_fasta} \
   --sjdbGTFfile ${annotation_gtf} \
   --sjdbOverhang ${star_oh}
  """
}

// Align the trimmed reads
process STAR_align {
  conda "bioconda::star=2.7.2b"

  cpus "10"
  memory "200GB"
  clusterOptions "-V -l h_vmem=20G"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(read1), file(read2) from trimmed_extracted_paired_fastq_ch2
  each file(index) from star_index_ch

  output:
  set sample_id, file("STAR/${sample_id}/Aligned.sortedByCoord.out.bam") into star_aligned_sorted_ch1, star_aligned_sorted_ch2, star_aligned_sorted_ch3
  set sample_id, file("STAR/${sample_id}/Aligned.toTranscriptome.out.bam") into star_aligned_to_transcriptome_ch
  set sample_id, file("STAR/${sample_id}/ReadsPerGene.out.tab") into star_reads_per_gene_ch
  set sample_id, file("STAR/${sample_id}/Log.final.out") into star_log_final_ch
  set sample_id, file("STAR/${sample_id}/Unmapped.out.mate1"), file("STAR/${sample_id}/Unmapped.out.mate2") into star_unmapped_paired_ch
  file "STAR/${sample_id}/Log.out"

  """
  mkdir -pv "./STAR/${sample_id}"
  STAR \
    --runThreadN 12 \
    --genomeDir ${index} \
    --readFilesIn ${read1} ${read2} \
    --readFilesCommand zcat \
    --outFilterType BySJout \
    --outFilterMultimapNmax 20 \
    --alignSJoverhangMin 8 \
    --alignSJDBoverhangMin 1 \
    --outFilterMismatchNmax 999 \
    --outFilterMismatchNoverLmax 0.6 \
    --alignIntronMin 20 \
    --alignIntronMax 1000000 \
    --alignMatesGapMax 1000000 \
    --outFileNamePrefix "./STAR/${sample_id}/" \
    --outSAMattributes NH HI NM MD AS nM \
    --outSAMtype BAM SortedByCoordinate \
    --quantMode TranscriptomeSAM GeneCounts \
    --outSAMattrRGline ID:${sample_id} LB:${sample_id} PU:unknown PL:illumina \
    --outReadsUnmapped Fastx;
  """
}

// Index the alignments for deduplication and visualization
process samtools_index {
  conda "bioconda::samtools=1.9"

  cpus "1"
  memory "10GB"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(sorted_bam) from star_aligned_sorted_ch1

  output:
  set sample_id, file("STAR/${sample_id}/${file_name}"), file("STAR/${sample_id}/${file_name}.bai") into bam_index_ch

  script:
  file_name = sorted_bam.getName()
  """
  mkdir -pv "./STAR/${sample_id}"
  samtools index ${sorted_bam}
  mv -v ${file_name}.bai ${sorted_bam} "./STAR/${sample_id}/"
  """
}

// Calculate the reference sequence GC
process bamstats_gc {
  conda "bioconda::samtools=1.9"

  cpus "1"
  memory "10GB"

  input:
  file genome_fasta from genome_egfp_ch2

  output:
  file "${reference_name}.gc" into reference_gc_ch

  script:
  reference_name = genome_fasta.getName()
  """
  plot-bamstats -s ${genome_fasta} >"./${reference_name}.gc"
  """
}

// Calculate the statistics for the alignment
process samtools_stats {
  conda "bioconda::samtools=1.9"

  cpus "1"
  memory "10GB"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(sorted_bam) from star_aligned_sorted_ch2
  each file(genome_fasta) from genome_egfp_ch3

  output:
  set sample_id, file("STAR/${sample_id}/${file_name}.bc") into samtools_stats_ch1, samtools_stats_ch2

  script:
  file_name = sorted_bam.getName()
  """
  mkdir -pv "./STAR/${sample_id}"
  samtools stats \
    -r ${genome_fasta} \
    ${sorted_bam} \
    >"./STAR/${sample_id}/${file_name}.bc";
  """
}

// Plot the calculated statistics
process plot_bamstats {
  conda "bioconda::samtools=1.9 conda-forge::gnuplot=5.0.6"

  cpus "1"
  memory "10GB"
  queue "ngs.q@c43"

  publishDir params.out_dir, mode: "copy"

  input:
  set sample_id, file(stats_file) from samtools_stats_ch1
  each file(reference_gc) from reference_gc_ch

  output:
  set sample_id, "bamstats/${sample_id}/*" into bamstats_ch

  """
  plot-bamstats \
    -p "bamstats/${sample_id}/${sample_id}" \
    -r ${reference_gc} \
    ${stats_file}
  """
}

// Perform deduplication based on UMI using the directional method
process umi_dedup {
  conda "bioconda::umi_tools=1.0.0"

  cpus "1"
  memory "20GB"
  clusterOptions "-V -l h_vmem=20G"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(sorted_bam), file(bam_index) from bam_index_ch

  output:
  file "${output_dir}/${sample_id}*.tsv" into umi_dedup_stats_ch
  file "${output_dir}/${sample_id}.log" into umi_dedup_log_ch
  file "${output_dir}/${sample_id}.err"
  set sample_id, file("${output_dir}/${sample_id}.umi_dedup.sorted.bam") into deduplicated_bam_ch1, deduplicated_bam_ch2

  script:
  output_dir = "umi_tools_dedup/${sample_id}"
  """
  mkdir -pv ${output_dir}
  umi_tools dedup \
    --output-stats="${output_dir}/${sample_id}" \
    --edit-distance-threshold=1 \
    --buffer-whole-contig \
    --multimapping-detection-method=NH \
    --paired \
    --stdin=${sorted_bam} \
    --log="${output_dir}/${sample_id}.log" \
    --error="${output_dir}/${sample_id}.err" \
    --stdout="${output_dir}/${sample_id}.umi_dedup.sorted.bam"; 
  """
}

// Index deduplicated alignments
process samtools_index_dedup {
  conda "bioconda::samtools=1.9"

  cpus "1"
  memory "10GB"

  publishDir params.out_dir, mode: "rellink"

  input:
  set sample_id, file(sorted_bam) from deduplicated_bam_ch1

  output:
  file("${output_dir}/${file_name}.bai")

  script:
  file_name = sorted_bam.getName()
  output_dir = "./umi_tools_dedup/${sample_id}"
  """
  mkdir -pv "${output_dir}"
  samtools index ${sorted_bam}
  mv -v ${file_name}.bai "${output_dir}"
  """
}

// Perform read quantification on the deduplicated alignments
process htseq_count {
  conda "bioconda::htseq=0.11.2"

  cpus "1"
  memory "20GB"
  clusterOptions "-V -l h_vmem=20G"

  publishDir params.out_dir, mode: "copy"

  input:
  set sample_id, file(sorted_bam) from deduplicated_bam_ch2
  each file(annotation_gtf) from annotation_egfp_ch2

  output:
  file "./htseq_count/${sample_id}_counts.tsv" into counts_ch

  """
  mkdir -v "./htseq_count"
  htseq-count \
    --format bam \
    --order pos \
    --stranded yes \
    --a 10 \
    --type exon \
    --idattr gene_id \
    --mode union \
    ${sorted_bam} \
    ${annotation_gtf} \
    >"./htseq_count/${sample_id}_counts.tsv";
  """
}

process rename_for_multiqc {
  cpus 1
  memory "10GB"

  input:
  set sample_id, star_file from star_reads_per_gene_ch.mix(star_log_final_ch, samtools_stats_ch2)

  output:
  file "${sample_id}${file_name}" into star_stats_ch

  script:
  file_name = star_file.getName()
  """
  cp -v ${star_file} ${sample_id}${file_name}
  """
}

process multiqc_processed {
  conda "bioconda::multiqc=1.7"

  cpus "1"
  memory "10GB"
  clusterOptions "-V -l h_vmem=10G"

  publishDir params.out_dir, mode: "copy"

  input:
  file "cutadapt/*" from cutadapt_log_ch.collect()
  file "fastqc/*" from fastqc_procesed_ch.collect()
  file "STAR/*" from star_stats_ch.collect()
  file "htseq_count/*" from counts_ch.collect() 

  output:
  file "multiqc_processed/multiqc_report.html"
  file "multiqc_processed/multiqc_data"

  """
  mkdir -v "./multiqc_processed"
  multiqc -o "./multiqc_processed" .
  """
}

// Prepare BLAST database for the homology searches
process make_blast_db {
  conda "bioconda::blast=2.9.0"
  
  cpus 1
  memory "10GB"

  storeDir params.reference_dir

  input:
  set file(cdna_fasta), file(ncrna_fasta) from reference_fasta_ch.collect()

  output:
  set alias_name, file("blastdb/{${cdna_name},${ncrna_name},${alias_name}}.*") into blastdb_ch

  script:
  cdna_name = cdna_fasta.baseName
  ncrna_name = ncrna_fasta.baseName
  // Create the database name from
  // <organism_name>.<assembly_version>.<annotation_version>
  alias_name = (cdna_name =~ /(^.+?(\..+?){2})\./)[0][1] + ".allrna"
  """
  mkdir -v "./blastdb"

  makeblastdb 
    -in "$cdna_fasta" \
    -parse_seqids \
    -dbtype nucl \
    -title "$cdna_name" \
    -out "./blastdb/$cdna_name";
  makeblastdb 
    -in "$ncrna_fasta" \
    -parse_seqids \
    -dbtype nucl \
    -title "$ncrna_name" \
    -out "./blastdb/$ncrna_name";
  blastdb_aliastool 
    -dblist "./blastdb/$cdna_name ./blastdb/$ncrna_name" \
    -dbtype nucl \
    -title "$alias_name" \
    -out "./blastdb/$alias_name";
  """
}

// Align the crRNA to the reference sequences
process blast {
  conda "bioconda::blast=2.9.0"

  cpus 10
  memory "20GB"
  clusterOptions "-V -l h_vmem=2G"

  publishDir params.out_dir, mode: "copy"

  input:
  set alias_name, file(blastdb) from blastdb_ch
  file crrna from crrna_ch

  output:
  file "blast/crrna_blast.tsv"

  """
  mkdir -v "./blast"

  blastn \
    -task blastn \
    -dust no \
    -evalue 1000 \
    -word_size 4 \
    -db "$alias_name" \
    -outfmt "6 sacc evalue bitscore length nident mismatch gaps sstrand" \
    -num_threads 10 \
    -subject_besthit \
    -max_hsps 1 \
    -max_target_seqs 10000 \
    -query "$crrna" \
    -out "./blast/crrna_blast.tsv"
  """
}

// vim: filetype=groovy tw=0
